import request from '@/utils/request'

export function userLog_list(query) {
    return request({
        url: '/userLog/list',
        method: 'post',
        params: query
    })
}
