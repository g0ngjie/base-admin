
import Layout from '@/views/layout/Layout'


const searchBar = {
    path: 'searchBar',
    component: Layout,
    redirect: 'searchBar',
    meta: { title: '搜索条', icon: 'iconfont ali-search' },
    children: [
        {
            path: '/baseSearch',
            component: () => import('@/views/searchBar/base'),
            name: 'BaseSearchBar',
            meta: { title: '基础搜索条', noCache: false }
        },
        {//试一试
            path: '/trySearch',
            component: () => import('@/views/searchBar/trySearch'),
            name: 'TrySearch',
            meta: { title: '试试看', noCache: false }
        },
        {
            path: '/otherSearch',
            component: () => import('@/views/searchBar/complex'),
            name: 'ComplexSearchBar',
            meta: { title: '复杂搜索条', noCache: false }
        }
    ]
}

export default searchBar;