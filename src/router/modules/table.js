
import Layout from '@/views/layout/Layout'

const table = {
    path: 'dataTable',
    component: Layout,
    redirect: 'dataTable',
    meta: { title: '表格', icon: 'iconfont ali-table' },
    children: [
        {
            path: '/baseTable',
            component: () => import('@/views/table/base'),
            name: 'BaseTable',
            meta: { title: '基础表格', noCache: false }
        },
        {
            path: '/tryTable',
            component: () => import('@/views/table/tryTable'),
            name: 'TryTable',
            meta: { title: '试试看', noCache: false }
        },
        {
            path: '/complexTable',
            component: () => import('@/views/dialog/complex'), 
            name: 'ComplexTable',
            meta: { title: '复杂表格' },
            children: [
                {
                    path: '/btnTable',
                    component: () => import('@/views/table/complex/btnTable'),
                    name: 'BtnTable',
                    meta: { title: '操作表格', noCache: true }
                },
                {
                    path: '/funcTable',
                    component: () => import('@/views/table/complex/funcTable'),
                    name: 'FuncTable',
                    meta: { title: '功能表格', noCache: true }
                },
                {//详情
                    path: '/tableDetail',
                    component: () => import('@/views/table/complex/funcTable/detail'),
                    hidden: true
                },
                {
                    path: '/treeTable',
                    component: () => import('@/views/table/complex/tree'),
                    name: 'TreeTable',
                    meta: { title: '树形表格', noCache: true }
                }
            ]
        }
    ]
}

export default table;