
import Layout from '@/views/layout/Layout'


const pagination = {
    path: 'pagination',
    component: Layout,
    redirect: 'pagination',
    meta: { title: '分页', icon: 'iconfont ali-chaifenyemian' },
    children: [
        {
            path: '/pagination',
            component: () => import('@/views/pagination'),
            name: 'Pagination',
            meta: { title: '分页', noCache: false }
        }
    ]
}

export default pagination;