
import Layout from '@/views/layout/Layout'


const chart = {
    path: 'chart',
    component: Layout,
    redirect: 'chart',
    meta: { title: '图表', icon: 'iconfont ali-tubiao' },
    children: [
        {
            path: '/chart',
            component: () => import('@/views/chart'),
            name: 'Chart',
            meta: { title: '图表', noCache: false }
        }
    ]
}

export default chart;