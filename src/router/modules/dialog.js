
import Layout from '@/views/layout/Layout'


const dialog = {
    path: '/dialog',
    component: Layout,
    redirect: 'dialog',
    meta: { title: '模态框', icon: 'iconfont ali-motaikuang' },
    children: [
        {
            path: '/baseDialog',
            component: () => import('@/views/dialog/base'),
            name: 'BaseDialog',
            meta: { title: '基础模态框', noCache: false }
        },
        {
            path: '/tryDialog',
            component: () => import('@/views/dialog/tryDialog'),
            name: 'TryDialog',
            meta: { title: '试试看', noCache: false }
        },
        {
            path: '/complexDialog',
            component: () => import('@/views/dialog/complex'), 
            name: 'ComplexDialog',
            meta: { title: '复杂模态框' },
            children: [
                {
                    path: '/tabsDialog',
                    component: () => import('@/views/dialog/complex/tabs'),
                    name: 'TabsDialog',
                    meta: { title: '标签页模态', noCache: false },
                },
                {
                    path: '/changeDialog',
                    component: () => import('@/views/dialog/complex/change'),
                    name: 'ChangeDialog',
                    meta: { title: '动态模态', noCache: false },
                }
            ]
        }
    ]
}

export default dialog;