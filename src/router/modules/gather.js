
import Layout from '@/views/layout/Layout'


const gather = {
    path: 'gather',
    component: Layout,
    redirect: 'gather',
    meta: { title: '综合', icon: 'iconfont ali-others' },
    children: [
        {
            path: '/gather',
            component: () => import('@/views/gather'),
            name: 'Gather',
            meta: { title: '综合', noCache: false }
        }
    ]
}

export default gather;