
import Layout from '@/views/layout/Layout'


const toolbar = {
    path: 'toolbar',
    component: Layout,
    redirect: 'toolbar',
    meta: { title: '工具栏', icon: 'iconfont ali-gongjulanmoban' },
    children: [
        {
            path: '/basebar',
            component: () => import('@/views/toolbar/base'),
            name: 'Basebar',
            meta: { title: '工具栏', noCache: false }
        },
        {
            path: '/tryToolbar',
            component: () => import('@/views/toolbar/tryToolbar'),
            name: 'TryToolbar',
            meta: { title: '试试看', noCache: false }
        }
    ]
}

export default toolbar;