import { asyncRouterMap, constantRouterMap } from '@/router'

const permission = {
  state: {
    routers: constantRouterMap,
    addRouters: [],
    routerLoadDone: false
  },
  mutations: {
    SET_ROUTERS: (state, routers) => {
      state.addRouters = routers
      state.routers = constantRouterMap.concat(routers)
      state.routerLoadDone = true;
    }
  },
  actions: {
    async generateRoutes({ commit }) {
      let accessedRouters = asyncRouterMap;
      commit('SET_ROUTERS', accessedRouters)
    }
  }
}

export default permission
