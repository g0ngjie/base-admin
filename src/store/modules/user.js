import { loginByUsername, logout, getUserInfo } from '@/api/login'
import { getToken, setToken, removeToken } from '@/utils/auth'

const user = {
  state: {
    user: '',
    token: getToken(),
    name: '',
    avatar: '',
    introduction: '',
  },

  mutations: {
    SET_TOKEN: (state, token) => {
      state.token = token
    },
    SET_INTRODUCTION: (state, introduction) => {
      state.introduction = introduction
    },
    SET_NAME: (state, name) => {
      state.name = name
    },
    SET_AVATAR: (state, avatar) => {
      state.avatar = avatar
    }
  },
  
  actions: {
    // 用户名登录
    async userLogin({ commit }, userInfo) {
      const username = userInfo.username.trim()
      const { data } = await loginByUsername(username, userInfo.password);
      commit('SET_TOKEN', data.token)
      setToken(data.token)
    },
    // 登出
    userLogOut({ commit, state }) {
      return new Promise((resolve, reject) => {
        logout(state.token).then(() => {
          commit('SET_TOKEN', '')
          removeToken()
          resolve()
        }).catch(error => {
          reject(error)
        })
      })
    },
    async getUserInfo({ commit }){
      const { data } = await getUserInfo()
      commit('SET_NAME', data.name)
      commit('SET_AVATAR', data.avatar)
      commit('SET_INTRODUCTION', data.introduction)
    },
    // 前端 登出
    async fedLogOut({ commit }) {
        commit('SET_TOKEN', '')
        removeToken()
    }
  }
}

export default user
