
import Mock from 'mockjs'
import { param2Obj } from '@/utils'


function getData(count){
  const List = []
  for (let i = 0; i < count; i++) {
    List.push(Mock.mock({
      id: '@increment',
      create_time: '@date()',
      user_name: '@cname',
      company_id: '@id(100)',
      module: '@id(10)',
      action: '@id(10)',
      behavior_desc: "@ctitle",
      consume_status: '@id(10)',
      'score|1-100': 100
    }))
  }
  return List;
}

export default {
  userLog_list: params => {
    const { start, limit } = param2Obj(params.url)
    const List = getData(limit)
    return {
      status: 100,
      data: {
        rows: List,
        results: 983
      }
    };
  }
}

