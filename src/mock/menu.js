export const menu_listAll = () => { //模拟菜单
  return {
    "status": 100,
    "msg": "SUCCESS",
    "data": {
      "rows": [{
        "close_status": 1,
        "company_id": 1,
        "create_time": "2018-11-29 14:39:25",
        "parent_id": 0,
        "menu_name": "通讯录",
        "menu_url": "1",
        "menu_code": "addressbook",
        "id": 452
      }, {
        "close_status": 1,
        "company_id": 1,
        "create_time": "2018-11-29 14:39:25",
        "parent_id": 1,
        "menu_name": "组织架构",
        "menu_url": "10",
        "menu_code": "addressbook_orgmain",
        "id": 453
      }, {
        "close_status": 1,
        "company_id": 1,
        "create_time": "2018-11-29 14:39:26",
        "parent_id": 10,
        "menu_name": "组织架构",
        "menu_url": "100",
        "menu_code": "orgmain_organize",
        "id": 454
      }, {
        "close_status": 1,
        "company_id": 1,
        "create_time": "2018-11-29 14:39:26",
        "parent_id": 10,
        "menu_name": "小组",
        "menu_url": "101",
        "menu_code": "orgmain_group",
        "id": 455
      }, {
        "close_status": 1,
        "company_id": 1,
        "create_time": "2018-11-29 14:39:27",
        "parent_id": 0,
        "menu_name": "论坛活动",
        "menu_url": "2",
        "menu_code": "activity",
        "id": 456
      }, {
        "close_status": 1,
        "company_id": 1,
        "create_time": "2018-11-29 14:39:27",
        "parent_id": 2,
        "menu_name": "活动管理",
        "menu_url": "20",
        "menu_code": "activity_activityManage",
        "id": 457
      }, {
        "close_status": 1,
        "company_id": 1,
        "create_time": "2018-11-29 14:39:27",
        "parent_id": 2,
        "menu_name": "社区管理",
        "menu_url": "21",
        "menu_code": "activity_communityManage",
        "id": 458
      }, {
        "close_status": 1,
        "company_id": 1,
        "create_time": "2018-11-29 14:39:28",
        "parent_id": 2,
        "menu_name": "消息发布",
        "menu_url": "22",
        "menu_code": "activity_publishMsg",
        "id": 459
      }, {
        "close_status": 1,
        "company_id": 1,
        "create_time": "2018-11-29 14:39:28",
        "parent_id": 2,
        "menu_name": "优化建议",
        "menu_url": "23",
        "menu_code": "activity_advice",
        "id": 460
      }, {
        "close_status": 1,
        "company_id": 1,
        "create_time": "2018-11-29 14:39:28",
        "parent_id": 0,
        "menu_name": "积分成就",
        "menu_url": "3",
        "menu_code": "achievement",
        "id": 461
      }, {
        "close_status": 1,
        "company_id": 1,
        "create_time": "2018-11-29 14:39:29",
        "parent_id": 3,
        "menu_name": "行为积分",
        "menu_url": "30",
        "menu_code": "achievement_action",
        "id": 462
      }, {
        "close_status": 1,
        "company_id": 1,
        "create_time": "2018-11-29 14:39:29",
        "parent_id": 3,
        "menu_name": "积分类别是的",
        "menu_url": "31",
        "menu_code": "achievement_integralType",
        "id": 463
      }, {
        "close_status": 1,
        "company_id": 1,
        "create_time": "2018-11-29 14:39:30",
        "parent_id": 3,
        "menu_name": "勋章分组",
        "menu_url": "32",
        "menu_code": "achievement_medalGroup",
        "id": 464
      }, {
        "close_status": 1,
        "company_id": 1,
        "create_time": "2018-11-29 14:39:30",
        "parent_id": 3,
        "menu_name": "成就勋章",
        "menu_url": "33",
        "menu_code": "achievement_medal",
        "id": 465
      }, {
        "close_status": 1,
        "company_id": 1,
        "create_time": "2018-11-29 14:39:30",
        "parent_id": 3,
        "menu_name": "积分统计",
        "menu_url": "34",
        "menu_code": "achievement_integralCount",
        "id": 466
      }, {
        "close_status": 1,
        "company_id": 1,
        "create_time": "2018-11-29 14:39:31",
        "parent_id": 34,
        "menu_name": "积分统计",
        "menu_url": "300",
        "menu_code": "integralCount_integralCount",
        "id": 467
      }, {
        "close_status": 1,
        "company_id": 1,
        "create_time": "2018-11-29 14:39:31",
        "parent_id": 0,
        "menu_name": "其他管理",
        "menu_url": "4",
        "menu_code": "other",
        "id": 468
      }, {
        "close_status": 1,
        "company_id": 1,
        "create_time": "2018-11-29 14:39:31",
        "parent_id": 4,
        "menu_name": "抽奖",
        "menu_url": "40",
        "menu_code": "other_draw",
        "id": 469
      }, {
        "close_status": 1,
        "company_id": 1,
        "create_time": "2018-11-29 14:39:32",
        "parent_id": 4,
        "menu_name": "问卷调查",
        "menu_url": "41",
        "menu_code": "other_question",
        "id": 470
      }, {
        "close_status": 1,
        "company_id": 1,
        "create_time": "2018-11-29 14:39:32",
        "parent_id": 4,
        "menu_name": "投票",
        "menu_url": "42",
        "menu_code": "other_vote",
        "id": 471
      }, {
        "close_status": 1,
        "company_id": 1,
        "create_time": "2018-11-29 14:39:33",
        "parent_id": 0,
        "menu_name": "系统设置",
        "menu_url": "5",
        "menu_code": "sysSet",
        "id": 472
      }, {
        "close_status": 1,
        "company_id": 1,
        "create_time": "2018-11-29 14:39:33",
        "parent_id": 5,
        "menu_name": "企业设置",
        "menu_url": "50",
        "menu_code": "sysSet_enterprise",
        "id": 473
      }, {
        "close_status": 1,
        "company_id": 1,
        "create_time": "2018-11-29 14:39:33",
        "parent_id": 50,
        "menu_name": "基础设置",
        "menu_url": "500",
        "menu_code": "enterprise_enterprisesSet",
        "id": 474
      }, {
        "close_status": 1,
        "company_id": 1,
        "create_time": "2018-11-29 14:39:34",
        "parent_id": 50,
        "menu_name": "通讯录设置",
        "menu_url": "501",
        "menu_code": "enterprise_mailList",
        "id": 475
      }, {
        "close_status": 1,
        "company_id": 1,
        "create_time": "2018-11-29 14:39:34",
        "parent_id": 50,
        "menu_name": "模板选择",
        "menu_url": "502",
        "menu_code": "enterprise_template",
        "id": 476
      }, {
        "close_status": 1,
        "company_id": 1,
        "create_time": "2018-11-29 14:39:34",
        "parent_id": 50,
        "menu_name": "编辑首页",
        "menu_url": "503",
        "menu_code": "enterprise_editIndex",
        "id": 477
      }, {
        "close_status": 1,
        "company_id": 1,
        "create_time": "2018-11-29 14:39:35",
        "parent_id": 5,
        "menu_name": "权限设置",
        "menu_url": "51",
        "menu_code": "sysSet_authority",
        "id": 478
      }, {
        "close_status": 1,
        "company_id": 1,
        "create_time": "2018-11-29 14:39:35",
        "parent_id": 51,
        "menu_name": "角色管理",
        "menu_url": "510",
        "menu_code": "authority_role",
        "id": 479
      }, {
        "close_status": 1,
        "company_id": 1,
        "create_time": "2018-11-29 14:39:35",
        "parent_id": 51,
        "menu_name": "菜单列表",
        "menu_url": "511",
        "menu_code": "authority_menu",
        "id": 480
      }, {
        "close_status": 1,
        "company_id": 1,
        "create_time": "2018-11-29 14:39:36",
        "parent_id": 5,
        "menu_name": "其他设置",
        "menu_url": "52",
        "menu_code": "sysSet_otherFit",
        "id": 481
      }, {
        "close_status": 1,
        "company_id": 1,
        "create_time": "2018-11-29 14:39:36",
        "parent_id": 52,
        "menu_name": "签到管理",
        "menu_url": "520",
        "menu_code": "otherFit_singnIn",
        "id": 482
      }, {
        "close_status": 1,
        "company_id": 1,
        "create_time": "2018-11-29 14:39:37",
        "parent_id": 52,
        "menu_name": "福利祝福",
        "menu_url": "521",
        "menu_code": "otherFit_welfare",
        "id": 483
      }, {
        "close_status": 1,
        "company_id": 1,
        "create_time": "2018-11-29 14:39:37",
        "parent_id": 0,
        "menu_name": "日志查看",
        "menu_url": "6",
        "menu_code": "log",
        "id": 484
      }, {
        "close_status": 1,
        "company_id": 1,
        "create_time": "2018-11-29 14:39:37",
        "parent_id": 6,
        "menu_name": "行为管理",
        "menu_url": "60",
        "menu_code": "log_actionManage",
        "id": 485
      }, {
        "close_status": 1,
        "company_id": 1,
        "create_time": "2018-11-29 14:39:38",
        "parent_id": 6,
        "menu_name": "用户日志",
        "menu_url": "61",
        "menu_code": "log_userLog",
        "id": 486
      }, {
        "close_status": 1,
        "company_id": 1,
        "create_time": "2018-11-29 14:39:38",
        "parent_id": 6,
        "menu_name": "系统日志",
        "menu_url": "62",
        "menu_code": "log_sysLog",
        "id": 487
      }]
    }
  }
}

export const menu_edit = () => {
  return {
    status: 100
  }
}
