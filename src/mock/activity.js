export const activity_list = () => {
  return {
    "status": 100,
    "msg": "SUCCESS",
    "data": {
      "rows": [{
        "file_path": "https://saic-public.oss-cn-shanghai.aliyuncs.com/1543928518903.jpeg",
        "close_status": 1,
        "article_type": 1,
        "comment_nums": 0,
        "company_id": 1,
        "create_time": "2018-12-04 21:02:34",
        "top_status": 1,
        "like_nums": 0,
        "read_nums": 1,
        "audit_status": 0,
        "comment_status": true,
        "browse_nums": 0,
        "create_username": "曹雪芹",
        "article_title": "提议改进食堂伙食，点赞的小伙伴high起来",
        "collect_nums": 0,
        "create_user": 123,
        "id": 39
      }, {
        "file_path": "https://saic-public.oss-cn-shanghai.aliyuncs.com/1542641009126.jpg",
        "close_status": 1,
        "comment_nums": 3,
        "company_id": 1,
        "create_time": "2018-11-19 23:24:12",
        "top_status": 0,
        "like_nums": 2,
        "read_nums": 56,
        "audit_status": 1,
        "comment_status": false,
        "browse_nums": 68,
        "create_username": "系统管理员",
        "article_title": "上汽斯柯达柯珞克 售价15-20万，你会买吗？",
        "collect_nums": 3,
        "create_user": 1,
        "id": 37
      }, {
        "file_path": "https://saic-public.oss-cn-shanghai.aliyuncs.com/1542640747017.jpg",
        "close_status": 1,
        "comment_nums": 0,
        "company_id": 1,
        "create_time": "2018-11-19 23:19:11",
        "top_status": 0,
        "like_nums": 1,
        "read_nums": 32,
        "audit_status": 1,
        "comment_status": true,
        "browse_nums": 40,
        "create_username": "系统管理员",
        "article_title": "上汽集团整体亮相北京车展 彰显竞争实力",
        "collect_nums": 2,
        "create_user": 1,
        "id": 35
      }],
      "results": 3
    }
  }
}

export const activity_findById = () => {
  return {
    "status": 100,
    "msg": "SUCCESS",
    "data": {
      "file_path": "https://saic-public.oss-cn-shanghai.aliyuncs.com/1543928518903.jpeg",
      "close_status": 1,
      "article_type": 1,
      "comment_nums": 0,
      "company_id": 1,
      "create_time": "2018-12-04 21:02:34",
      "article_content": "<p>干的漂亮！！</p><img src=\"https://saic-public.oss-cn-shanghai.aliyuncs.com/1543928551281.jpeg\" style=\"max-width:100%;\">",
      "top_status": 1,
      "like_nums": 0,
      "read_nums": 1,
      "audit_status": 0,
      "articleComments": [],
      "comment_status": 1,
      "browse_nums": 0,
      "create_username": "曹雪芹",
      "article_title": "提议改进食堂伙食，点赞的小伙伴high起来",
      "collect_nums": 0,
      "create_user": 123,
      "id": 39
    }
  }
}
