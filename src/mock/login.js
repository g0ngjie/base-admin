import { param2Obj } from '@/utils'

const userMap = {
  admin: {
    roles: ['admin'],
    token: 'admin',
    introduction: '我是超级管理员',
    avatar: './static/avatar.jpg',
    name: 'Super Admin'
  }
}

export default {
  loginByUsername: config => {
    return userMap['admin']
  },
  getUserInfo: config => {
    return userMap['admin']
  },
  logout: () => 'success'
}
