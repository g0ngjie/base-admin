
exports.install = function (Vue, options) {
  
  //验证手机号
  const isvalidateMobile= (rule, value, callback) => {        
    if(value != null && value != "") {
      if(!isPoneAvailable(value)) {
          callback(new Error('您输入的手机号不正确!'))
      } else {
          callback()
      }
    }
    else{
        callback();
    }
  }

  function isPoneAvailable($poneInput) {  
    var myreg=/^[1][3,4,5,7,8][0-9]{9}$/;  
    if (!myreg.test($poneInput)) {  
        return false;  
    } else {  
        return true;  
    }  
  } 
    
  //请输入正整数
  const isvalidateInteger= (rule, value, callback) => {    
    if(value != null && value != "") {
      if(typeof value === 'number' && value >= 0 && value%1 === 0) { 
      　　callback()
      }else {
          callback(new Error('请输入大于零的整数!'))
      }
    }
    else{
        callback();
    }
  }


  /**
   * 参数 item 
   * required true  必填项
   * maxLength  字符串的最大长度
   * min 和 max 必须同时给 min < max  type=number
   * type 手机号 mobile
   *      邮箱   email
   *      网址   url 
   *      各种自定义类型   定义在 src/utils/validate 中    持续添加中.......
   * */

  Vue.prototype.filter_rules = function (item){
    let rules = [];
    if(item.required){
      rules.push({ required: true, message: '该输入项为必填项!', trigger: 'blur' });
    }
    if(item.maxLength){
      rules.push({ min:1,max:item.maxLength, message: '最多输入'+item.maxLength+'个字符!', trigger: 'blur' })
    }
    if(item.min&&item.max){       
      rules.push({ min:item.min,max:item.max, message: '字符长度在'+item.min+'至'+item.max+'之间!', trigger: 'blur' })
    }
    if(item.type){
      let type = item.type;
      switch(type) {
        case 'email':
            rules.push({ type: 'email', message: '请输入正确的邮箱地址', trigger: 'blur,change'  });
            break;
        case 'upload':
            rules.push( { required: true, message: '请上传文件!', trigger: 'change' });
            break;
        case 'mobile':
            rules.push( { validator: isvalidateMobile, trigger: 'blur,change' });
            break;    
        case 'number':
            rules.push( { validator: isvalidateInteger, trigger: 'blur' });
            break;        
        default:
            rule.push({});
            break;
      }
    }
    return rules;
  }
}

