
import { Message } from 'element-ui'

export const toolbars = [
    {
        el: 'iv-button',
        option: {
            text: '添加',
            func() {
                Message.warning("执行添加")
            }
        }
    },
    {
        el: 'iv-button',
        option: {
            text: '编辑',
            type: 'warning',
            plain: true,
            func() {
                Message("执行编辑")
            }
        }
    },
    {
        el: 'iv-button',
        option: {
            text: '导出',
            round: true,
            type: 'success',
            func() {
                Message.success("导出成功")
            }
        }
    },
    {
        el: 'iv-button',
        option: {
            text: '查看',
            type: 'danger',
            round: true,
            func() {
                Message.success("查看")
            }
        }
    },
    {
        el: 'iv-span',
        span: 4,
        option: {
            text: '总数量10',
            style: {
                color: '#008080',
                'font-size': '15px',
                'margin-left': '15px'
            }
        }
    },
]