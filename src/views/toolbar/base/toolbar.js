
import { Message } from 'element-ui'

export const toolbars1 = [
    {
        el: 'iv-button',
        option: {
            text: '添加',
            type: 'warning',
            func() {
                Message.warning("执行添加")
            }
        }
    },
    {
        el: 'iv-button',
        option: {
            text: '编辑',
            func() {
                Message("执行编辑")
            }
        }
    },
    {
        el: 'iv-button',
        option: {
            text: '导出',
            type: 'success',
            func() {
                Message.success("导出成功")
            }
        }
    },
    {
        el: 'iv-upload-btn',
        option: {
            text: '表格导入',
            type: 'info',
            action: '/upload/upload',
            name: 'file_path',
            before(file) {
                let name = file.name.split('.');
                if (name[name.length - 1] === 'xls' || name[name.length - 1] === 'xlsx') {
                    return file
                } else {
                    Message.warning('请上传正确的格式!')
                    return false
                }
            },
            success(response, file, fileList) {
                if(response.code === 100){
                    Message.success(`${file.name} 上传成功`)
                }
            },
            error(err, file, fileList) {
                Message.error("上传出现错误")
            }
        }
    },
    {
        el: 'iv-span',
        span: 4,
        option: {
            text: '总数量10',
            style: {
                color: '#008080',
                'font-size': '15px',
                'margin-left': '15px'
            }
        }
    },
]

export const toolbars2 = [
    {
        el: 'iv-button',
        option: {
            text: '添加',
            type: 'warning',
            plain: true,
            func() {
                Message.warning("执行添加")
            }
        }
    },
    {
        el: 'iv-button',
        option: {
            text: '编辑',
            plain: true,
            func() {
                Message("执行编辑")
            }
        }
    },
    {
        el: 'iv-button',
        option: {
            text: '导出',
            type: 'success',
            plain: true,
            func() {
                Message.success("导出成功")
            }
        }
    },
    {
        el: 'iv-upload-btn',
        option: {
            text: '表格导入',
            type: 'info',
            plain: true,
            action: '/upload/upload',
            name: 'file_path',
            before(file) {
                let name = file.name.split('.');
                if (name[name.length - 1] === 'xls' || name[name.length - 1] === 'xlsx') {
                    return file
                } else {
                    Message.warning('请上传正确的格式!')
                    return false
                }
            },
            success(response, file, fileList) {
                if(response.code === 100){
                    Message.success(`${file.name} 上传成功`)
                }
            },
            error(err, file, fileList) {
                Message.error("上传出现错误")
            }
        }
    }
]

export const toolbars3 = [
    {
        el: 'iv-button',
        option: {
            text: '添加',
            type: 'warning',
            round: true,
            func() {
                Message.warning("执行添加")
            }
        }
    },
    {
        el: 'iv-button',
        option: {
            text: '编辑',
            round: true,
            func() {
                Message("执行编辑")
            }
        }
    },
    {
        el: 'iv-button',
        option: {
            text: '导出',
            type: 'success',
            round: true,
            func() {
                Message.success("导出成功")
            }
        }
    },
    {
        el: 'iv-upload-btn',
        option: {
            text: '表格导入',
            type: 'info',
            round: true,
            action: '/upload/upload',
            name: 'file_path',
            before(file) {
                let name = file.name.split('.');
                if (name[name.length - 1] === 'xls' || name[name.length - 1] === 'xlsx') {
                    return file
                } else {
                    Message.warning('请上传正确的格式!')
                    return false
                }
            },
            success(response, file, fileList) {
                if(response.code === 100){
                    Message.success(`${file.name} 上传成功`)
                }
            },
            error(err, file, fileList) {
                Message.error("上传出现错误")
            }
        }
    }
]

export const toolbars4 = [
    {
        el: 'iv-button',
        option: {
            text: '添加',
            type: 'text',
            round: true,
            func() {
                Message.warning("执行添加")
            }
        }
    },
    {
        el: 'iv-button',
        option: {
            text: '编辑',
            type: 'text',
            func() {
                Message("执行编辑")
            }
        }
    },
    {
        el: 'iv-button',
        option: {
            text: '导出',
            type: 'text',
            func() {
                Message.success("导出成功")
            }
        }
    },
    {
        el: 'iv-upload-btn',
        option: {
            text: '表格导入',
            type: 'text',
            action: '/upload/upload',
            name: 'file_path',
            before(file) {
                let name = file.name.split('.');
                if (name[name.length - 1] === 'xls' || name[name.length - 1] === 'xlsx') {
                    return file
                } else {
                    Message.warning('请上传正确的格式!')
                    return false
                }
            },
            success(response, file, fileList) {
                if(response.code === 100){
                    Message.success(`${file.name} 上传成功`)
                }
            },
            error(err, file, fileList) {
                Message.error("上传出现错误")
            }
        }
    }
]