
import { Message } from 'element-ui'

export const columns = {
    fields: [                           //模态框需要字段 泛指 form表单下的 label、input、value、validate等
        {                               //字符串类型input
            el    : 'str-input',        //元素 str 
            label : '用户名',            //form-item => label
            value : 'userName',         //input value
            rule  : { required: true }, //是否表单验证 填写类： require: true,type: upload 等
            option: {}                  //涉及到 元素的基本配置 如 width 等。如若没有，需定义空
        },
        {
            el    : 'num-input',
            label : '年龄',
            value : 'age',
            rule  : { required: true, type: 'number' },
            option: {}
        },
        {
            el    : 'iv-textarea',
            label : '备注',
            value : 'desc',
            option: {}
        },
        {
            el    : 'iv-radio',
            label : '性别',
            value : 'sex',
            option: {
                radios: [
                    {
                        label: 1,
                        value: '男'
                    },
                    {
                        label: 0,
                        value: '女'
                    }
                ]
            }
        },
        {
            el    : 'iv-check-box',
            label : '技能',
            value : 'skill',
            option: {
                checkboxs: [
                    {
                        label: 'spring',
                        value: 'spring'
                    },
                    {
                        label: 'mybatis',
                        value: 'mybatis'
                    },
                    {
                        label: 'maven',
                        value: 'maven'
                    },
                    {
                        label: 'git',
                        value: 'git'
                    },
                    {
                        label: 'mysql',
                        value: 'mysql'
                    },
                    {
                        label: 'oracle',
                        value: 'oracle'
                    },
                    {
                        label: 'hibernate',
                        value: 'hibernate'
                    },
                    {
                        label: 'linux',
                        value: 'linux'
                    }
                ]
            }
        },
        {
            el    : 'iv-select',
            label : '职位',
            value : 'job',
            option: {
                placeholder: '请选择职位',
                selects: [
                    {
                        label: '前端工程师',
                        value: 0
                    },
                    {
                        label: '服务端工程师',
                        value: 1
                    },
                    {
                        label: '架构师',
                        value: 2
                    },
                    {
                        label: '销售顾问',
                        value: 3
                    },
                    {
                        label: '设计师',
                        value: 4
                    },
                    {
                        label: '实施',
                        value: 5
                    },
                    {
                        label: '运维工程师',
                        value: 6
                    }
                ]
            }
        },
        {
            el    : 'iv-date-picker',
            label : '入职时间',
            value : 'joinDate',
            option: {
                type: 'date',
            }
        },
        {
            el    : 'iv-upload',
            label : '头像',
            value : 'avatar',
            option: {
                width : '110px',
                herght: '110px',
                action: '/upload/upload',
                before: (res, file) => {
                    let name = res.name.split('.');
                    if (name[name.length - 1] === 'jpg' || name[name.length - 1] === 'jpeg' || name[name.length - 1] === 'png') {
                        return res
                    } else {
                        Message.warning('图片只能是 jpeg/png 格式!')
                        return false
                    }
                },
                success: async res => {
                    if (res.code == 100) {
                        return res.data.url
                    }
                }
            }
        }
    ]
}