
import { Message, MessageBox } from 'element-ui'

export const tableColumn = () => {
    const table = {
        selection: true,
        index: true,
        tr : [
            {
                label: "行为描述",
                prop: "behavior_desc"
            },
            {
                label: "操作人",
                prop: "user_name"
            },
            {
                label: "操作时间",
                prop: "create_time"
            },
            {
                label: "分数",
                prop: "score"
            },
            {
                el: 'button',
                label: '操作',
                btns: [
                    {
                        text: '编辑',
                        func: row => {
                            Message(row.behavior_desc)
                        }
                    },
                    {
                        text: '删除',
                        type: 'danger',
                        func: async row => {
                            try {
                                let res = await MessageBox.confirm('确认删除吗？')
                                if (res == 'confirm') {
                                    Message.success('删除成功')
                                }
                            } catch (error) {
                                console.log(error, 'error')
                            }
                        }
                    }
                ]
            }
        ]
    }
    return table;
}
