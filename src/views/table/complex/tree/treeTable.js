

export const tableColumn = (editById, openDialog) => {
    const columns = [{
            text: '菜单',
            value: 'menu_name',
            width: 200
        },
        {
            text: 'CODE',
            value: 'menu_code'
        },
        {
            text: 'ID',
            value: 'menu_url'
        },
        {
            text: 'PARENT_ID',
            value: 'parent_id'
        },
        {
            text: '状态',
            value: 'isClose'
        },
        {
            text: '操作',
            check: 'double',
            double: [{
                value: 'btn',
                el: 'button',
                func: row => {
                    editById(row);
                }
            },{
                value: 'edit',
                el: 'button',
                func: row => {
                    openDialog(row)
                }
            }]
        }
    ]
    return columns;
}

export const toolBarColumn = expandAll => {
    const fields = [
        {
            el: 'iv-button',
            option: {
                text: '展开',
                func: expandAll
            }
        }
    ]
    return fields;
}

export const dialogColumn = {
    fields: [                           
        {                              
            el    : 'str-input',        
            label : '名称',            
            value : 'menu_name',         
            rule  : { required: true }, 
            option: { width: '200px'}                  
        }
    ]
}