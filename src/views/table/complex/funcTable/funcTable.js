
import { Message, MessageBox } from 'element-ui'

const audit_status = [{value: '待审核',type: 'warning'}, {value: '通过',type: 'success'}, {value: '未通过',type: 'danger'}]

export const tableColumn = (link) => {
    const table = {
        index: true,
        tr : [
            {
                el: 'icon',
                width: '50px',
                icon: {
                    key: 'top_status',
                    class: 'iconfont ali-zhiding',
                    condition: 1,
                    style: {color: '#009900', 'font-size': '20px'}
                }
            },
            {
                el: 'link',
                label: "主题",
                link: {
                    type: 'text',
                    key: 'article_title',
                    func: (row) => {
                        link(row)
                    }
                }
            },
            {
                label: "作者",
                prop: "create_username"
            },
            {
                label: "发布时间",
                prop: "create_time"
            },
            {
                label: "评论量",
                prop: "comment_nums"
            },
            {
                label: "点赞量",
                prop: "like_nums"
            },
            {
                label: "浏览量",
                prop: "browse_nums"
            },
            {
                el: 'switch',
                label: "评论",
                switch: {
                    key: "comment_status",
                    func: (row, event) => {
                        console.log(row, 'row')
                        console.log(event, 'event')
                    }
                }
            },
            {
                el: 'tag',
                label: "状态",
                tags: [
                    {
                        key: 'audit_status',
                        format: audit_status
                    }
                ]
            },
            {
                el: 'button',
                label: '操作',
                btns: [
                    {
                        text: '通过',
                        type: 'text',
                        func: row => {
                            Message('操作成功')
                        }
                    },
                    {
                        text: '不通过',
                        type: 'text',
                        func: async row => {
                            try {
                                let res = await MessageBox.confirm('审核确认？')
                                if (res == 'confirm') {
                                    Message.success('审核成功')
                                }
                            } catch (error) {
                                console.log(error, 'error')
                            }
                        }
                    },
                    {
                        text: '删除',
                        type: 'text',
                        func: async row => {
                            try {
                                let res = await MessageBox.confirm('确认删除吗？')
                                if (res == 'confirm') {
                                    Message.success('删除成功')
                                }
                            } catch (error) {
                                console.log(error, 'error')
                            }
                        }
                    }
                ]
            }
        ]
    }
    return table;
}
