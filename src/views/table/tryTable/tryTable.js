
export const tableColumn = {
    index: true,
    selection: true,
    tr: [{
        label: "行为",
        prop: "action"
    },
    {
        label: "模块",
        prop: "module"
    },
    {
        label: "行为描述",
        prop: "behavior_desc"
    },
    {
        label: "操作人",
        prop: "user_name"
    },
    {
        label: "操作时间",
        prop: "create_time"
    },
    {
        label: "分数",
        prop: "score"
    }]
}

export const tableData = [{
    "create_time": "2018-12-05 13:02:58",
    "user_name": "张三",
    "module": 2,
    "score": 0,
    "action": 6,
    "behavior_desc": "帖子被阅读"
}, {
    "create_time": "2018-12-05 13:02:57",
    "user_name": "李四",
    "module": 2,
    "score": 5,
    "action": 1,
    "behavior_desc": "阅读帖子"
}]
  