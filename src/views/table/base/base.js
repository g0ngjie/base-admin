


export const tableColumn = () => {
  const table = {
    index: true,
    tr: [
       {
         label: "行为",
         prop: "action"
       },
       {
         label: "模块",
         prop: "module"
       },
       {
         label: "行为描述",
         prop: "behavior_desc"
       },
       {
         label: "操作人",
         prop: "user_name"
       },
       {
         label: "操作时间",
         prop: "create_time"
       },
       {
         label: "分数",
         prop: "score"
       }
     ]
  }
  return table;
}
