import { Message, MessageBox } from 'element-ui'

export const toolbars = (open, span, delAll, autoScroll) => {
    return [
        {
            el: 'iv-button',
            option: {
                text: '添加',
                func: open
            }
        },
        {
            el: 'iv-button',
            option: {
                text: '回滚',
                func: autoScroll
            }
        },
        {
            el: 'iv-button',
            option: {
                text: '批量删除',
                type: 'danger',
                func: delAll
            }
        },
        {
            el: 'iv-span',
            span: 5, 
            option: {
                text: span,
                style: {
                    color: '#008080',
                    'font-size': '15px',
                    'margin-left': '20px'
                }
            }
        }
    ]
}
