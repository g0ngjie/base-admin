
import { Message } from 'element-ui'

export const dialogs = {
    fields: [                           
        {                               
            el    : 'str-input',       
            label : '行为描述',           
            value : 'behavior_desc',        
            rule  : { required: true }, 
            option: {}                 
        },
        {                               
            el    : 'str-input',       
            label : '操作人',           
            value : 'user_name',        
            rule  : { required: true }, 
            option: {}                 
        },
        {
            el    : 'iv-date-picker',
            label : '操作时间',
            value : 'create_time',
            option: {
                type: 'date',
            }
        },
        {
            el    : 'num-input',
            label : '分数',
            value : 'score',
            rule  : { required: true, type: 'number' },
            option: {
                width : '220px'
            }
        }
    ]
}