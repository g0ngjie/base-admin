
import { Message } from 'element-ui'

export const initColumns = search => {
    const columns =  [                           
            {                               
                el    : 'str-input',        
                label : '操作人',           
                value : 'user_name',        
                option: {
                    width: '200px',
                    placeholder: "请输入操作人"
                }                 
            },
            {
                el    : 'num-input',
                label : '分数',
                value : 'age',
                option: {
                    width: '200px',
                    placeholder: "请输入分数"
                }
            },
            {
                el    : 'iv-date-picker',
                label : '操作时间',
                value : 'create_time',
                option: {
                    width: '300px',
                    type: 'daterange',
                }
            },
            {
                el    : 'iv-button',
                option: {
                    text: "搜索",
                    func: search
                }
            }
        ]
    return columns;
}