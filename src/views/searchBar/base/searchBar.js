
import { Message } from 'element-ui'

export const initColumns = (search) => {
    const columns = [                           
        {                               
            el    : 'str-input',        
            label : '姓名',           
            value : 'userName',        
            option: {
                width: '200px'
            }                 
        },
        {                               
            el    : 'str-input',        
            label : '工号',           
            value : 'jobNumber',        
            option: {
                width: '200px'
            }                 
        },
        {                               
            el    : 'str-input',        
            label : '备注',           
            value : 'desc',        
            option: {
                width: '200px'
            }                 
        },
        {
            el    : 'iv-button',
            option: {
                text: "搜索",
                func: search
            }
        }
    ]
    return columns;
}