
import { Message } from 'element-ui'

export const initColumns1 = (search, clear) => {
    const columns = [                           
        {                               
            el    : 'str-input',        
            label : '请购人',           
            value : 'startPerson',        
            option: {
                width: '200px'
            }                 
        },
        {
            el    : 'str-input',
            label : '审核人',
            value : 'examinePerson',
            option: {
                width: '200px'
            }
        },
        {
            el    : 'iv-select',
            label : '库存组织',
            value : 'stockOrg',
            option: {
                selects: [
                    {
                        label: '加油机',
                        value: '加油机'
                    },
                    {
                        label: '售服',
                        value: '售服'
                    },
                    {
                        label: '物联',
                        value: '物联'
                    },
                    {
                        label: '整站',
                        value: '整站'
                    },
                    {
                        label: '行政中心',
                        value: '行政中心'
                    }
                ]
            }
        },
        {
            el    : 'iv-select',
            label : '请购类型',
            value : 'startType',
            option: {
                selects: [
                    {
                        label: '采购中心采购请购',
                        value: '采购中心采购请购'
                    },
                    {
                        label: '固定资产采购请购',
                        value: '固定资产采购请购'
                    },
                    {
                        label: '自行采购请购',
                        value: '自行采购请购'
                    },
                    {
                        label: '办公用品请购',
                        value: '办公用品请购'
                    },
                    {
                        label: '安全物资请购',
                        value: '安全物资请购'
                    }
                ]
            }
        },
        {
            el    : 'iv-select',
            label : '审核状态',
            value : 'examineStatus',
            option: {
                selects: [
                    {
                        label: '未审核',
                        value: '未审核'
                    },
                    {
                        label: '通过',
                        value: '通过'
                    },
                    {
                        label: '拒绝',
                        value: '拒绝'
                    }
                ]
            }
        },
        {
            el    : 'iv-date-picker',
            label : '请购日期',
            value : 'requestDate',
            option: {
                type: 'daterange',
            }
        },
        {
            el    : 'iv-button',
            option: {
                text: "筛选",
                func: search
            }
        },
        {
            el    : 'iv-button',
            option: {
                text: "清空",
                func: clear
            }
        }
    ]
    return columns;
}

export const initColumns2 = search => {
    const columns = [                           
        {                               
            el    : 'str-input',        
            label : '用户名',           
            value : 'userName',        
            option: {
                width: '200px'
            }                 
        },
        {
            el    : 'num-input',
            label : '年龄',
            value : 'age',
            option: {
                width: '200px'
            }
        },
        {
            el    : 'iv-select',
            label : '职位',
            value : 'job',
            option: {
                placeholder: '请选择职位',
                selects: [
                    {
                        label: '前端工程师',
                        value: '前端工程师'
                    },
                    {
                        label: '服务端工程师',
                        value: '服务端工程师'
                    },
                    {
                        label: '架构师',
                        value: '架构师'
                    },
                    {
                        label: '销售顾问',
                        value: '销售顾问'
                    },
                    {
                        label: '设计师',
                        value: '设计师'
                    },
                    {
                        label: '实施',
                        value: '实施'
                    },
                    {
                        label: '运维工程师',
                        value: '运维工程师'
                    }
                ]
            }
        },
        {
            el    : 'iv-date-picker',
            label : '入职时间',
            value : 'joinDate',
            option: {
                type: 'date',
            }
        },
        {
            el    : 'iv-button',
            option: {
                text: "搜索",
                func: search
            }
        }
    ]
    return columns;
}