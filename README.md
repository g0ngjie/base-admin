# base-admin

## 整理一套Web后台的基础架构代码。
![search_try](https://images.gitee.com/uploads/images/2018/1217/131941_dcd644b5_1437187.gif "search_try.gif")
######   项目参考 vue-element-admin

- <a href="http://admin.ivday.fun" target="_blank">在线访问</a>
- <a href="https://gitee.com/gjwork/base-admin/wikis/pages" target="_blank">文档</a>

### 目标
> 为解决前端开发当中部分重复性劳动力,把更多的精力放在交互上。
> 
> 后台管理可以分为几个模块
> - layout(整体布局)
>   + Header头部
>   + Menu菜单
>   + Main页面
> #### 细化Main可分为
> - 搜索栏
>
>   <img src="static/search.jpg" width="700">
>
> - 工具栏
>
>   <img src="static/toolbar.jpg" width="530">  
>
> - 表格/图表
>
>   <img src="static/table.jpg" width="700">
>
> - 分页
>
>   <img src="static/pagination.jpg" width="600">
>
> - 模态框
>
>   <img src="static/dialog.jpg" width="430">
> 

### 实现
> 常用功能都以抽象成组件。实际开发当中只需要按照固定格式，
> 编写少量的配置文件即可实现页面的搭建。为开发节省了大量的
> 布局时间。实现敏捷式开发。
### 目录

```
│  favicon.ico
│  index.html
│  LICENSE
│  package.json
│  README.md
├─build 
├─config      
├─node_modules                   
├─src
│  │  App.vue
│  │  main.js           主入口
│  │  permission.js     全局路由守卫
│  ├─api                服务端接口文件
│  ├─assets             资源文件（包括icon）
│  │  └─icon        
│  ├─components         抽象出来的组件
│  ├─mock               模拟服务端请求数据
│  ├─router             路由配置文件
│  ├─store              vuex文件
│  ├─styles             全局样式集合
│  ├─utils              抽象出来的常用工具
│  └─views              模板页面
└─static                静态资源文件

```
### 总结
> 多数都是实际开发过程当中总结出来的经验。很多场景实际上还没有覆盖，
> 后续会持续迭代。

### 准备工作
```
git clone git@gitee.com:gjwork/base-admin.git
```
### 安装&运行
 - 推荐使用 yarn 安装，执行以下命令
```
yarn
yarn dev
```
 - 如果使用 npm，可执行 ***npm install && npm run dev***，效果一致。